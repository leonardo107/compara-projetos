function parseTxtContent(content) {
    const lines = content.trim().split('\n');
    const nomeConst = lines[0].trim();
    const columns = lines[1].split('\t').map(col => col.trim());

    const projects = lines.slice(2).map(line => {
        const values = line.split('\t').map(value => value.trim());
        const projectObj = {};
        columns.forEach((col, index) => {
            projectObj[col] = values[index];
        });
        return projectObj;
    });

    const result = {};
    result[nomeConst] = { "projetos": projects };
    return result;
}

function parseDate(dateString) {
    return new Date(dateString);
}

function compararProjetos() {
    const web4Content = document.getElementById('web4TextArea').value;
    const demoContent = document.getElementById('demoTextArea').value;

    const web4 = parseTxtContent(web4Content);
    const demo = parseTxtContent(demoContent);

    const projetosWeb4 = web4[Object.keys(web4)[0]].projetos.reduce((map, projeto) => {
        map[projeto.Projeto] = parseDate(projeto['Ultimo Deploy']);
        return map;
    }, {});

    const projetosDemo = demo[Object.keys(demo)[0]].projetos.reduce((map, projeto) => {
        map[projeto.Projeto] = parseDate(projeto['Ultimo Deploy']);
        return map;
    }, {});

    const projetosDesatualizados = Object.keys(projetosDemo).filter(projeto =>
        projetosWeb4[projeto] && projetosDemo[projeto] < projetosWeb4[projeto]
    );

    const resultadoUl = document.getElementById('resultado');
    resultadoUl.innerHTML = '';
    projetosDesatualizados.forEach(projeto => {
        const li = document.createElement('li');
        li.textContent = projeto;
        resultadoUl.appendChild(li);
    });
}
